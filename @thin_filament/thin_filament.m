classdef thin_filament < handle
    
    properties
        no_of_bs;
        default_bs_spacing;
        regulatory_unit_size;
        on_indices;
        k_a;
        x;
        status;
        bound;
        k_on;
        k_off;
        k_plus;
    end
    
    properties (SetAccess = private)
        
    end
    
    methods
        % Constructor
         function thin = thin_filament(varargin)
            % Defaults
            params.thin_data = [];
            
            % Update
            params = parse_pv_pairs(params,varargin);
            
            % Copy data
            thin_field_names = fieldnames(params.thin_data);
            for i=1:numel(thin_field_names)
                thin.(thin_field_names{i}) = ...
                    params.thin_data.(thin_field_names{i});
            end
            
            % Initialise binding site positions and status
            for i=1:thin.no_of_bs
                thin.x(i) = i * thin.default_bs_spacing;
                thin.status(i) = 0;
                thin.bound(i) = 0;
            end
         end      
       
         % Thin filament kinetics
         function thin = run_kinetics(thin,time_step,Ca_conc)
             
             no_of_units = thin.no_of_bs / thin.regulatory_unit_size;
             
             if (0)
             
                 % Find units that are
                 % 1) off
                 % 2) bound
                 % 3) on and unbound

                 actin_bound_i = find(thin.bound>0);
                 unit_bound_i = ceil(actin_bound_i./thin.regulatory_unit_size);

                 actin_first_indices = 1:thin.regulatory_unit_size:thin.no_of_bs;
                 unit_on_i = find(thin.status(actin_first_indices)>0);
                 unit_off_i = find(thin.status(actin_first_indices)==0);

                 unit_on_and_unbound = setdiff(unit_on_i,unit_bound_i);

                 % Sites can turn on
                 if (~isempty(unit_off_i))
                     on_rand = rand(numel(unit_off_i),1);
                     vi = find(on_rand > exp(-(thin.k_on * Ca_conc * ...
                                                time_step)));
                     for i=1:numel(vi)
                        ui = unit_off_i(vi(i));
                        ai = ((ui-1)*thin.regulatory_unit_size) + ...
                            (1:thin.regulatory_unit_size);
                        thin.status(ai) = 1;
                     end
                 end

                 % Sites can turn off
                 if (~isempty(unit_on_and_unbound))
                     off_rand = rand(numel(unit_on_and_unbound),1);
                     vi = find(off_rand > exp(-(thin.k_off * time_step)));

                     for i=1:numel(vi)
                         ui = unit_on_and_unbound(vi(i));
                         ai = ((ui-1)*thin.regulatory_unit_size) + ...
                                (1:thin.regulatory_unit_size);
                         thin.status(ai) = 0;
                     end
                 end
             else

                 if (isempty(thin.on_indices))
                     bs_indices=1:thin.regulatory_unit_size;
                 else
                     bs_indices=thin.on_indices;
                 end

                 for i=1:no_of_units
                     bs_i = (i-1)*thin.regulatory_unit_size + ...
                                (1:thin.regulatory_unit_size);

                     if (all(thin.status(bs_i)==0))
                         % Find if any adjacent sites are boound
                         on_counter=0;
                         if (i>1)
                             left_indices = bs_i - thin.regulatory_unit_size;
                             if (any(thin.bound(left_indices)))
                                 on_counter=on_counter+1;
                             end
                         end
                         if (i<no_of_units)
                             right_indices = bs_i + thin.regulatory_unit_size;
                             if (any(thin.bound(right_indices)))
                                 on_counter=on_counter+1;
                             end
                         end
                         % unit is off, could turn on
                         on_rand = rand;
                         on_value = exp(-(thin.k_on * Ca_conc) * ...
                                (1 + (on_counter * thin.k_plus)) * ...
                                time_step);
                         if (on_rand > on_value)
                             thin.status(bs_i(bs_indices)) = 1;
                         end
                     else
                         if (~any(thin.bound(bs_i)>0))
                            % unit is on, but could turn off
                            off_rand = rand;
                            if (off_rand > exp(-thin.k_off * time_step))
                                thin.status(bs_i) = 0;
                            end
                         end
                     end
                 end
             end
         end
             
    end
end
        