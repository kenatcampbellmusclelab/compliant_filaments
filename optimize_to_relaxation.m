function optimize_to_relaxation

global best;

p =  [200   20    4   -2.5   5e6   10     0];
lb = [10    0.2   2    -6   1e6   10      0]
b = [400  50     8     -2   5e6   50     50];

% Wipe file
of = fopen('c:\temp\out.txt','w');
fclose(of);

% Load data
[sim_data,target_f] = ...
    distributed_generate_target_pmid_17123098_ventricular;

sim_data.no_of_repeats=5;
single_repeat=1;

% Create best structure
best.p = [];
best.e = [];
best.f = [];

if (~single_repeat)
    % Create handle to simulation
    fh = @(p)return_fit_error(p,sim_data,target_f);

    [best_p,best_e] = particle_swarm_optimization( ...
        fh, ...
        'initial_p',p, ...
        'lower_bounds',lb, ...
        'upper_bounds',ub, ...
        'figure_parameters',21);
else
    return_fit_error(p,sim_data,target_f);
end

end

function e = return_fit_error(p,sim_data,target_f)

    global best

    thick_data.f = p(1);
    thick_data.g = [p(2) p(3) p(4) 5];
    thick_data.k_on = p(5);
    thick_data.k_off = p(6);
    thin_data.k_plus = p(7);
    
    d = simulation_driver( ...
        'sim_data.no_of_repeats',sim_data.no_of_repeats, ...
        'sim_data.n',sim_data.n, ...
        'sim_data.Ca_conc',sim_data.Ca_conc, ...
        'sim_data.time_step',sim_data.time_step, ...   
        'sim_data.t',cumsum(sim_data.time_step), ...
        'thin_data.k_on',thick_data.k_on, ...
        'thin_data.k_off',thick_data.k_off, ...
        'thin_data.k_plus',thin_data.k_plus, ...
        'thick_data.k_cb',0.002, ...
        'thick_data.ps',5, ...
        'thick_data.f',thick_data.f, ...
        'thick_data.g',thick_data.g, ...
        'thick_data.g_shape','bilog', ...
        'thick_data.k_m',1*0.3*10^0, ...
        'thin_data.k_a',1*1.6*10^0, ...
        'thin_data.regulatory_unit_size',1, ...
        'hs_data.debug',0);
            
    f = mean(d.force,2);
    target_n = numel(target_f);
    vi = (numel(f) - numel(target_f)+1):numel(f);
    
    % Calculate error
    e = sum((f(vi)-target_f).^2);
    
    % Hold if required
    if (isempty(best.e))
        best.e = e;
        best.p = p;
        best.f = f;
    end
    
    if (e < best.e)
        best.e = e;
        best.p = p;
        best.f = f;
    end
    
    % Display
    figure(11);
    clf;
    hold on;
    plot(d.t(vi),target_f,'k-');
    plot(d.t(vi),f(vi),'b-');
    plot(d.t(vi),best.f(vi),'r-');
    
    % Log
    of = fopen('c:\temp\out.txt','a');
    fprintf(of,'e: %g\t p: ',e);
    for i=1:numel(p)
        fprintf(of,'%g\t',p(i));
    end
    fprintf(of,'\n');
    fclose(of);    
end
    
    
    
    
    
    
    
    