function holder = simulation_driver(varargin)

% Reset randon numbers
rng(1);

% Get default data
[hs_data,thick_data,thin_data] = default_parameters();
sim_data = default_simulation;

% Update
for i=1:2:nargin
    temp=textscan(varargin{i},'%s');
    struct_string=temp{:};
    values=varargin{i+1};

    if (numel(values)==1)
        value_string=sprintf('%g',values);
    else
        if (ischar(values))
            value_string = sprintf('''%s''',values);
        else
            value_string='[';
            for j=1:numel(values)
                value_string = sprintf('%s %g',value_string,values(j));
            end
            value_string = sprintf('%s]',value_string);
        end
    end
    eval_string = sprintf('%s = %s;',char(struct_string),value_string);
    eval(eval_string);
end

thick_data = thick_data
thin_data = thin_data
hs_data = hs_data

for i=1:sim_data.no_of_repeats
    progress_bar(i/sim_data.no_of_repeats,sprintf('Repeat %.0f',i));
    
    sim_data = run_simulation(sim_data,hs_data,thick_data,thin_data);
    
    holder.force(1:sim_data.n,i) = sim_data.force';
    holder.f_activated(1:sim_data.n,i) = sim_data.f_activated';
    holder.f_bound(1:sim_data.n,i) = sim_data.f_bound';
    holder.m_bound(1:sim_data.n,i) = sim_data.m_bound';
    holder.force(1:sim_data.n,i) = sim_data.force';
    holder.thick_strain(1:sim_data.n,i) = sim_data.thick_strain';
    holder.thin_strain(1:sim_data.n,i) = sim_data.thin_strain';
    holder.cb_dx(1:sim_data.n,i) = (sim_data.cb_dx)';
    holder.Ca_conc(1:sim_data.n,i) = (sim_data.Ca_conc)';
    
    figure(3);
    clf;
    nr=4;
    
    subplot(nr,1,1);
    plot(sim_data.t,sim_data.Ca_conc,'b-');

    subplot(nr,1,2);
    hold on;
    plot(sim_data.t,sum(holder.f_activated,2)./i,'b-');
    plot(sim_data.t, ...
        sum(holder.f_bound,2)./(i/thin_data.regulatory_unit_size), ...
        'b:');
    plot(sim_data.t,sum(holder.m_bound,2)./i,'r-');

    subplot(nr,1,3);
    plot(sim_data.t,sum(holder.force,2)./i,'b-');
    
    subplot(nr,1,4);
    hold on;
    plot(sim_data.t,sum(holder.thick_strain,2)./i,'b-');
    plot(sim_data.t,sum(holder.thin_strain,2)./i,'b:');
    legend('Thick','Thin');
end

holder.t = sim_data.t;