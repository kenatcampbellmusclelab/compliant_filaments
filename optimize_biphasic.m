function optimize_biphasic

p =  [100   20      4  -2   2e6   100   10];
lb = [1     0.2     1   -6  1e6     1   0];
ub = [500  100      6  -1  1e7    1000   100];

of = fopen('c:\temp\out.txt','w');
fclose(of);

[best_p,best_e] = particle_swarm_optimization( ...
    @return_slope_ratio, ...
    'initial_p',p, ...
    'lower_bounds',lb, ...
    'upper_bounds',ub, ...
    'figure_parameters',21);

end


function e = return_slope_ratio(p)

    thick_data.f = p(1);
    thick_data.g = [p(2) p(3) p(4) 5];
    thick_data.k_on = p(5);
    thick_data.k_off = p(6);
    thin_data.k_plus = p(7);
    
    vi=1001:1250;
    
    d = simulation_driver( ...
        'sim_data.no_of_repeats',20, ...
        'thin_data.k_on',thick_data.k_on, ...
        'thin_data.k_off',thick_data.k_off, ...
        'thin_data.k_plus',thin_data.k_plus, ...
        'thick_data.k_cb',0.002, ...
        'thick_data.f',thick_data.f, ...
        'thick_data.g',thick_data.g, ...
        'thick_data.g_shape','bilog', ...
        'thick_data.k_m',0.5, ...
        'thin_data.k_a',2);
            
    f = mean(d.force(vi,:),2);
    
    v_end = find(f<5000,1,'first');
    if (~isempty(v_end))
        vi = vi(1:v_end);
        f = f(1:v_end);
    end
    
    if (v_end<=100)
        e=10^1;
        return;
    end
 
    n=3;
    x=1:numel(vi);
    pol = polyfit(x,f',n);
    y_fit = polyval(pol,x);
%    
%     
%     b = BrokenStickRegression(vi,f,2);
%     xm = round(b(2,1));
%     
    figure(5);
    clf;
    hold on;
    plot(x,f,'b-');
    plot(x,y_fit,'r-');
%     plot([vi(1) xm],[b(1,2) b(2,2)],'r-');
%     plot([xm vi(end)],[b(2,2) b(3,2)],'r-');
%     
%     m1 = (b(2,2)-b(1,2))/(b(2,1)-b(1,1));
%     m2 = (b(3,2)-b(2,2))/(b(3,1)-b(2,1));
   
    diff_vector=diff(y_fit);
    e = 10^(diff_vector(end)./max(f))
    
    of = fopen('c:\temp\out.txt','a');
    fprintf(of,'e: %f p: ',e);
    for i=1:numel(p)
        fprintf(of,'%f\t',p(i));
    end
    fprintf(of,'\n');
    fclose(of);
end

function fit_error = exp_rise(p,x,y)
    y_fit = (exp(p*x)-1);
    fit_error = sum((y_fit-y').^2);
end
    
    
    
    
    
    
    
    