function hs = calculate_df_vector(hs)

    hs.df_vector = zeros(hs.no_of_nodes,1);
    
    % Find connections
    cb_vi = find(hs.thick.bound > 0);
    
    for i=1:numel(cb_vi)
        cb_i = cb_vi(i);
        bs_i = hs.thick.bound(cb_vi(i));
        
        cb_node = hs.thin.no_of_bs + cb_i;
        bs_node = bs_i;
        
        % Adjust df_vector
        hs.df_vector(bs_node) = hs.df_vector(bs_node) + ...
            hs.thick.k_cb * hs.thick.ps;
        
        hs.df_vector(cb_node) = hs.df_vector(cb_node) - ...
            hs.thick.k_cb * hs.thick.ps;
    end
end
