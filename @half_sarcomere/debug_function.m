function hs = debug(hs)

    % Find bound cbs
    vi = find(hs.thick.bound>0)
    
    cb_force = 0;
    
    dx = [];
    
    for i=1:numel(vi)
        cb_i = vi(i);
        bs_i = hs.thick.bound(vi(i));
        
        x = hs.thick.x(cb_i) - hs.thin.x(bs_i);
        dx = [dx x];
        
        cb_force = cb_force + (hs.thick.k_cb * (0*x + hs.thick.ps));
    end
    
    sprintf('cb_force %f  hs.force %f',cb_force,hs.force)
    
    
    figure(71);
    x=-6:0.5:6;
    [n,x]=hist(dx,x);
    bar(x,n);
end