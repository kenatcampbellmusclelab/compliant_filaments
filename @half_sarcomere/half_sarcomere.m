classdef half_sarcomere < handle
    
    properties
        hs_length;
        thick_filament_density;
        
        k_t;
        titin_slack_length;
                
        thick;
        thin;
        
        no_of_nodes;
        
        k_matrix;
        k0_matrix;
        f_vector;
        f0_vector;
        
        iteration_counter=0;
        t = 0;
        
        force;                      % force in nN
        initial_thick_end;
        initial_thin_end;        
        
        filament_force_factor;      % scales for geometry
        
        force_tolerance;
        
        debug;
        
        figure_filaments;
        draw_skip;
        draw_pause;
        
        figure_kinetics;
    end
    
    properties (SetAccess = private)
        
    end
    
    methods (Access = public)
        % Functions defined in separate files in the folder
        hs = hs_kinetics(hs,time_step);
        
        hs = initialize_k_matrix(hs);
        hs = initialize_f_vector(hs);
        hs = calculate_g_vector(hs,x);
        hs = calculate_df_vector(hs);
        
        hs = calculate_node_positions(hs);
        
        r = f_rate(hs,x);
        r = g_rate(hs,x);
    end
    
    methods (Access = public)
        % Functions defined in separate files in the folder
        hs = check_test(hs);
        hs = check_unbalanced_forces(hs);
    end
    
    methods
        % Constructor
        function hs = half_sarcomere(varargin)
            
            % Defaults
            params.hs_data = [];
            params.thick_data = [];
            params.thin_data = [];
            
            params = parse_pv_pairs(params,varargin);
            
            % Copy data
            hs_field_names = fieldnames(params.hs_data);
            for i=1:numel(hs_field_names)
                hs.(hs_field_names{i}) = ...
                    params.hs_data.(hs_field_names{i});
            end
            
            % Create thick and thin filament
            hs.thick = thick_filament('thick_data',params.thick_data);
            hs.thin = thin_filament('thin_data',params.thin_data);
            
            % Calculate nodes
            hs.no_of_nodes = hs.thick.no_of_cbs + hs.thin.no_of_bs;
            
            % Initialize f_vector and k_matrix
            hs = hs.initialize_k_matrix;
            hs.k0_matrix = hs.k_matrix;
%             hs.k_matrix_sparse = sparse(hs.k_matrix);
            hs = hs.initialize_f_vector;
            hs.f0_vector = hs.f_vector; 
           
            
            % Calculate initial node positions
            hs = hs.calculate_node_positions;
            
            % Store initial lengths
            hs.initial_thick_end = hs.thick.x(hs.thick.no_of_cbs);
            hs.initial_thin_end = hs.thin.x(hs.thin.no_of_bs);
        end
        
        % Implement time_step
        function hs = implement_time_step(hs,time_step,Ca_conc)
             
            hs.iteration_counter = hs.iteration_counter + 1;
            
            hs.t = hs.t + time_step;

            hs.thin = hs.thin.run_kinetics(time_step,Ca_conc);
            
            hs = hs.hs_kinetics(time_step);
      
            hs = hs.calculate_node_positions;
            
            hs = hs.calculate_force;

            if ((hs.figure_filaments>0) & ...
                    (mod(hs.iteration_counter,hs.draw_skip)==0))
                hs.draw_filaments;
            end
            
            if (hs.debug)
                hs.debug_function;
            end
        end
        
        % Calculate force
        function hs = calculate_force(hs)
            hs.force = hs.thick.k_m * ...
                (hs.thick.hs_length - hs.thick.lambda - ...
                    hs.thick.default_cb_spacing - hs.thick.x(1));
        end
        
        
        % Display
        function draw_filaments(hs)
            figure(hs.figure_filaments);
            clf;
            cla;
            hold on;
            % Draw Z-line
            plot([0 0],[0 3],'k-','LineWidth',3);
            % Draw cbs
            plot(hs.thick.x,2*ones(numel(hs.thick.x),1),'bo');
            % Draw links
            vi_links = find(hs.thick.bound > 0);
            for i=1:numel(vi_links)
                plot([hs.thick.x(vi_links(i)) ...
                            hs.thin.x(hs.thick.bound(vi_links(i)))], ...
                        [2 1],'k-');
            end
            % Draw binding sites
            vi_on = find(hs.thin.status==1);
            plot(hs.thin.x(vi_on),ones(numel(vi_on),1),'gs');
            vi_off = find(hs.thin.status==0);
            plot(hs.thin.x(vi_off),ones(numel(vi_off),1),'rs');
            % Draw M-line
            plot(hs.hs_length*[1 1],[0 3],'m-','LineWidth',3);
            % Annotations
            text(0,0,sprintf('%.3f s',hs.t), ...
                'HorizontalAlignment','left', ...
                'VerticalAlignment','bottom');
            % Pause
            pause(hs.draw_pause);
            drawnow;
        end
        
    end
end
        