function hs = calculate_g_vector(hs,x)

    hs.g_vector = zeros(hs.no_of_nodes,1);
    
    % Find connections
    cb_vi = find(hs.thick.bound > 0);
    
    for i=1:numel(cb_vi)
        cb_i = cb_vi(i);
        bs_i = hs.thick.bound(cb_vi(i));
        
        cb_node = hs.thin.no_of_bs + cb_i;
        bs_node = bs_i;
        
        % Adjust g_vector
        
        dx = x(cb_node) - x(bs_node);
        
        hs.g_vector(bs_node) = hs.g_vector(bs_node) + ...
            hs.thick.k_cb * x(bs_node) - hs.thick.k_cb * x(cb_node);
        
        hs.g_vector(cb_node) = hs.g_vector(cb_node) + ...
            hs.thick.k_cb * x(cb_node) - hs.thick.k_cb * x(bs_node);
    end
end
        
        
        