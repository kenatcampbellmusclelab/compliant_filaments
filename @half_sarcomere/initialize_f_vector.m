function hs = initialize_f_vector(hs)

hs.f_vector = zeros(hs.no_of_nodes,1);

hs.f_vector(hs.thin.no_of_bs) = hs.thin.k_a * hs.thin.default_bs_spacing;

hs.f_vector(hs.thin.no_of_bs + 1) = ...
    hs.thick.k_m * (hs.hs_length - hs.thick.lambda);

hs.f_vector(hs.no_of_nodes) = ...
    -hs.thick.k_m * hs.thick.default_cb_spacing + ...
    hs.k_t * hs.titin_slack_length;