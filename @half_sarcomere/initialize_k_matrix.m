function hs = initialize_k_matrix(hs)

hs.k_matrix = zeros(hs.no_of_nodes,hs.no_of_nodes);

for i=1:hs.thin.no_of_bs
    if (i==1)
        hs.k_matrix(i,i) = 2 * hs.thin.k_a;
        hs.k_matrix(i,i+1) = -hs.thin.k_a;
    end
    
    if ((i>1)&&(i<hs.thin.no_of_bs))
        hs.k_matrix(i,i-1) = -hs.thin.k_a;
        hs.k_matrix(i,i) = 2 * hs.thin.k_a;
        hs.k_matrix(i,i+1) = -hs.thin.k_a;
    end
    
    if (i==hs.thin.no_of_bs)
        hs.k_matrix(i,i-1) = -hs.thin.k_a;
        hs.k_matrix(i,i) = hs.thin.k_a;
    end
end

for i=1:hs.thick.no_of_cbs
    ind = hs.thin.no_of_bs + i;
    
    if (i==1)
        hs.k_matrix(ind,ind) = 2 * hs.thick.k_m;
        hs.k_matrix(ind,ind+1) = -hs.thick.k_m;
    end
    
    if ((i>1)&&(i<hs.thick.no_of_cbs))
        hs.k_matrix(ind,ind-1) = -hs.thick.k_m;
        hs.k_matrix(ind,ind) = 2 * hs.thick.k_m;
        hs.k_matrix(ind,ind+1) = -hs.thick.k_m;
    end
    
    if (i==hs.thick.no_of_cbs)
        hs.k_matrix(ind,ind-1) = -hs.thick.k_m;
        hs.k_matrix(ind,ind) = hs.thick.k_m + hs.k_t;
    end
end