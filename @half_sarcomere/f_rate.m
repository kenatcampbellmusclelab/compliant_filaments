function r = f_rate(hs,dx)
    r = hs.thick.f(1) * exp(-0.5 * hs.thick.k_cb * (dx^2) / ...
        (1e18 * 1.381e-23 * 288));
end
