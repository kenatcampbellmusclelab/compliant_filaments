function hs = calculate_node_positions(hs)

% Calculate node positions by calculating the k_matrix and the f_vector
% and then performing x = k\f
% This is quick if you take into account the fact that k is sparse

k = hs.k0_matrix;
f = hs.f0_vector;

% Find links
cb_i = find(hs.thick.bound > 0);
bs_i = hs.thick.bound(cb_i);

cb_node = hs.thin.no_of_bs + cb_i;
bs_node = bs_i;

for i=1:numel(cb_i)
    k(bs_node(i),bs_node(i)) = k(bs_node(i),bs_node(i)) + ...
        hs.thick.k_cb;
    k(bs_node(i),cb_node(i)) = k(bs_node(i),cb_node(i)) - ...
        hs.thick.k_cb;

    k(cb_node(i),cb_node(i)) = k(cb_node(i),cb_node(i)) + ...
        hs.thick.k_cb;
    k(cb_node(i),bs_node(i)) = k(cb_node(i),bs_node(i)) - ...
        hs.thick.k_cb;
end

for i=1:numel(cb_i)
    f(bs_node(i)) = f(bs_node(i)) + hs.thick.k_cb * hs.thick.ps;
    f(cb_node(i)) = f(cb_node(i)) - hs.thick.k_cb * hs.thick.ps;
end

k=sparse(k);

x = (k\f)';

% Unpack
hs.thin.x = x(1:hs.thin.no_of_bs);
hs.thick.x = x(hs.thin.no_of_bs+(1:hs.thick.no_of_cbs));
