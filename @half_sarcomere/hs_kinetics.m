function hs = hs_kinetics(hs,time_step)

    % Get attached cbs
    attached_cbs = find(hs.thick.bound > 0);
    unattached_cbs = find(hs.thick.bound == 0);
    
    % Do attached cbs detach
    % Vectorized approach
    if (1)
        cb_i = attached_cbs;
        
        if (numel(cb_i)>0)

            bs_i = hs.thick.bound(cb_i);
            dx = hs.thick.x(cb_i) - hs.thin.x(bs_i);
            g_values = hs.g_rate(dx);

            rand_values = rand(1,numel(g_values));

            vi = find(rand_values > exp(-time_step * g_values));

            % Adjust
            hs.thick.status(cb_i(vi)) = 0;
            hs.thick.bound(cb_i(vi)) = 0;

            hs.thin.bound(bs_i(vi)) = 0;
        end
    else
        % Loop approach
        for i=1:numel(attached_cbs)

            cb_i = attached_cbs(i);
            bs_i = hs.thick.bound(attached_cbs(i));

            dx = hs.thick.x(cb_i) - hs.thin.x(bs_i);

            if (rand > exp(-hs.g_rate(dx) * time_step))

                hs.thick.status(cb_i) = 0;
                hs.thick.bound(cb_i) = 0;

                hs.thin.bound(bs_i) = 0;
            end
        end
    end
    
    % Check whether detached cbs attach
    for i=1:numel(unattached_cbs)
        % Calculate distances to available sites
        available_sites = find(hs.thin.status == 1 & ...
                                hs.thin.bound == 0);
        
        if (numel(available_sites)>0)

            d = hs.thick.x(unattached_cbs(i)) - ...
                    hs.thin.x(available_sites);
            [dx,si] = sort(abs(d));
            
            if (rand > exp(-hs.f_rate(dx(1)) * time_step))
                % Attach to nearest site
                cb_i = unattached_cbs(i);
                bs_i = available_sites(si(1));

                hs.thick.status(cb_i) = 1;
                hs.thick.bound(cb_i) = bs_i;

                hs.thin.bound(bs_i) = cb_i;
            end
        end
    end
end