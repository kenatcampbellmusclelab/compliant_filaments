function r = g_rate(hs,dx)

switch hs.thick.g_shape
    case 'poly'
        for i=1:numel(dx)
            if (dx(i)>0)
                r(i) = hs.thick.g(1) + abs(hs.thick.g(2)*dx(i)^hs.thick.g(4));
            else
                r(i) = hs.thick.g(1) + abs(hs.thick.g(3)*dx(i)^hs.thick.g(5));
            end
        end
    case 'bilog'
        slope = hs.thick.g(4);
        max_value = 10000;
        for i=1:numel(dx)
            if (dx(i)>0)
                r(i) = hs.thick.g(1) + (max_value-hs.thick.g(1)) / ...
                    (1+exp(-slope * (dx(i)-hs.thick.g(2))));
            else
                r(i) = hs.thick.g(1) + (max_value-hs.thick.g(1)) / ...
                    (1+exp(slope * (dx(i)-hs.thick.g(3))));
            end
        end
    otherwise
        r(i) = zeros(numel(dx),1);
end
