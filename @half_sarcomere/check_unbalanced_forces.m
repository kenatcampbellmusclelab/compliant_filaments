function hs = check_unbalanced_forces(hs)

% Start by checking actin nodes
for i=1:hs.thin.no_of_bs
    if (i==1)
        uf(i) = hs.thin.k_a * (hs.thin.x(i+1)-hs.thin.x(i)-hs.thin.default_bs_spacing) - ...
                    hs.thin.k_a * (hs.thin.x(i) - hs.thin.default_bs_spacing);
    end
    
    if ((i>1)&(i<hs.thin.no_of_bs))
        uf(i) = hs.thin.k_a * (hs.thin.x(i+1) - hs.thin.x(i) - hs.thin.default_bs_spacing) - ...
            hs.thin.k_a * (hs.thin.x(i) - hs.thin.x(i-1) - hs.thin.default_bs_spacing);
    end

    if (i==hs.thin.no_of_bs)
        uf(i) = -hs.thin.k_a * (hs.thin.x(i) - hs.thin.x(i-1) - hs.thin.default_bs_spacing);
    end
end

% Now myosin nodes
for i=1:hs.thick.no_of_cbs
    ind = hs.thin.no_of_bs + i;
    if (i==1)
        uf(ind) = hs.thick.k_m * (hs.hs_length - hs.thick.lambda - hs.thick.x(i) - hs.thick.default_cb_spacing) - ...
            hs.thick.k_m * (hs.thick.x(i) - hs.thick.x(i+1) - hs.thick.default_cb_spacing);
    end
    
    if ((i>1) && (i<hs.thick.no_of_cbs))
        uf(ind) = hs.thick.k_m * (hs.thick.x(i-1) - hs.thick.x(i) - hs.thick.default_cb_spacing) - ...
            hs.thick.k_m * (hs.thick.x(i) - hs.thick.x(i+1) - hs.thick.default_cb_spacing);
    end
    
    if (i==hs.thick.no_of_cbs)
        uf(ind) = hs.thick.k_m * (hs.thick.x(i-1) - hs.thick.x(i) - hs.thick.default_cb_spacing) - ...
            hs.k_t * (hs.thick.x(i) - hs.titin_slack_length);
    end
end

% Add in cb links
vi = find(hs.thick.bound>0);
for i=1:numel(vi)
    cb_i = vi(i);
    bs_i = hs.thick.bound(cb_i);
    
    cb_node = hs.thin.no_of_bs + cb_i;
    bs_node = bs_i;
    
    dx = hs.thick.x(cb_i) - hs.thin.x(bs_i);
    
    uf(bs_node) = uf(bs_node) + hs.thick.k_cb * (dx + hs.thick.ps);
    uf(cb_node) = uf(cb_node) - hs.thick.k_cb * (dx + hs.thick.ps);
end

% Check
[max_unbalanced_force,node] = max(abs(uf));
if (max_unbalanced_force > hs.force_tolerance)
    
    [max_unbalanced_force,node] = max(abs(uf))
    
    figure(3);
    clf;
    subplot(3,1,1);
    plot(uf,'b-');
    ylabel('Unbalanced forces');
    subplot(3,1,2);
    plot(hs.df_vector,'b-');
    ylabel('df vector');
    subplot(3,1,3);
    plot(hs.g_vector,'b-');
    ylabel('g vector');

    
    error('Check forces failed');
end