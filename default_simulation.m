function sim_data= default_simulation;

sim_data.n = 1600;
sim_data.time_step = 0.001*ones(sim_data.n,1);
sim_data.Ca_conc = 1e-9*ones(sim_data.n,1);
sim_data.Ca_conc(100:1000) = 32e-6;
sim_data.t = cumsum(sim_data.time_step)';
sim_data.no_of_repeats = 3;