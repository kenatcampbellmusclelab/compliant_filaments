function sim_data = run_simulation(sim_data,hs_data,thick_data,thin_data)

% Create a half-sarcomere
hs = half_sarcomere( ...
    'hs_data',hs_data, ...
    'thick_data',thick_data, ...
    'thin_data',thin_data);

% Create a kinetics figure
if (hs.figure_kinetics)
    x=linspace(-8,8,101);
    for i=1:numel(x);
        f(i)=hs.f_rate(x(i));
        g(i)=hs.g_rate(x(i));
    end
    
    figure(hs.figure_kinetics);
    clf;
    subplot(2,1,1);
    if (0)
        plot(x,log10(f),'b-');
        ylim([-2 4]);
        subplot(2,1,2);
        plot(x,log10(g),'b-');
        xlim([min(x) max(x)]);
        ylim([-2 4]);
    else
        plot(x,f,'b-');
        ylim([0 1000]);
        subplot(2,1,2);
        plot(x,g,'b-');
        xlim([min(x) max(x)]);
        ylim([0 1000]);
    end
end

% Work out how many time-steps
n = numel(sim_data.time_step);

% Reserve space
sim_data.pCa = NaN*ones(1,n);
sim_data.force = NaN*ones(1,n);
sim_data.f_activated = NaN*ones(1,n);
sim_data.f_bound = NaN*ones(1,n);
sim_data.m_bound = NaN*ones(1,n);
sim_data.thick_strain = NaN*ones(1,n);
sim_data.thin_strain = NaN*ones(1,n);

% Cycle through time-steps
for i=1:n
    hs.implement_time_step( ...
        sim_data.time_step(i), ...
        sim_data.Ca_conc(i));
    
    if (i==n)
        hs.draw_filaments;
    end
    
    if (hs.force_tolerance>0)
        progress_bar(i/n,sprintf('Simulation time %.3f',hs.t));
        hs = hs.check_unbalanced_forces;
    end
    
    % Store data
    sim_data.force(i) = hs.force * hs.thick_filament_density * ...
                            1e-9 * hs.filament_force_factor;
        % Converts force from nN to N m^{-2}
        % and allows for 6 filaments
    sim_data.f_activated(i) = sum(hs.thin.status)/hs.thin.no_of_bs;
    sim_data.f_bound(i) = sum(hs.thin.bound > 0)/hs.thin.no_of_bs;
    sim_data.m_bound(i) = sum(hs.thick.bound > 0)/hs.thick.no_of_cbs;
    sim_data.thick_strain(i) = (hs.hs_length - ...
                                        hs.thick.x(hs.thick.no_of_cbs)) / ...
                                (hs.hs_length - hs.initial_thick_end);
    sim_data.thin_strain(i) = hs.thin.x(hs.thin.no_of_bs) / ...
                                    hs.initial_thin_end;
    % Find bound cbs
    vi = find(hs.thick.bound>0);
    sim_data.cb_dx{i} = hs.thick.x(vi) - hs.thin.x(hs.thick.bound(vi));
end
    

