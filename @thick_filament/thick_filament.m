classdef thick_filament < handle
    
    properties
        no_of_cbs;
        default_cb_spacing;
        hs_length;
        lambda;
        k_m;
        k_cb;
        ps;
        x;
        status;
        bound;
        f;
        g;
        g_shape;
    end
    
    properties (SetAccess = private)
        
    end
    
    methods
        % Constructor
        function thick = thick_filament(varargin)
            % Defaults
            params.thick_data = [];
            
            % Update
            params = parse_pv_pairs(params,varargin);
            
            % Copy data
            thick_field_names = fieldnames(params.thick_data);
            for i=1:numel(thick_field_names)
                thick.(thick_field_names{i}) = ...
                    params.thick_data.(thick_field_names{i});
            end
            
            % Initialise bin positions and status
            thick.lambda = thick.lambda + rand * thick.default_cb_spacing;
            for i=1:thick.no_of_cbs
                thick.x(i) = thick.hs_length - thick.lambda - ...
                    (i*thick.default_cb_spacing);
                thick.status(i) = 0;
                thick.bound(i) = 0;
            end
        end            
    end
end
        