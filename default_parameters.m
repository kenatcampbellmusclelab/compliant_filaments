function [hs_data,thick_data,thin_data] = default_parameters

hs_data.hs_length = 1100;
hs_data.k_t = 0.0001;
hs_data.titin_slack_length = 200;
hs_data.thick_filament_density = 0.407e15;
hs_data.filament_force_factor = 6;
% Linari et al. 2007 
hs_data.debug = 0;
hs_data.force_tolerance = -1e-10;
hs_data.figure_filaments = 1;
hs_data.draw_skip = 10000;
ha_data.draw_pause = 0.0;
hs_data.figure_kinetics=2;

thick_data.no_of_cbs = 18;
thick_data.default_cb_spacing = 42.9;
thick_data.lambda = 80;
thick_data.k_m = 0.25;
thick_data.hs_length = hs_data.hs_length;
thick_data.k_cb = 0.002;
thick_data.ps = 5;
thick_data.f = [50];
thick_data.g = [10 3*ones(1,2)];
thick_data.g_shape = 'poly';

thin_data.no_of_bs = 7 * 27;
thin_data.default_bs_spacing = 5.375;
thin_data.regulatory_unit_size = 7;
% thin_data.no_of_bs = 27;
% thin_data.default_bs_spacing = 7 * 5.375;
% thin_data.regulatory_unit_size = 1;

thin_data.on_indices = [];
thin_data.k_a = 1.7;
thin_data.k_on = 1e7;
thin_data.k_off = 100;
thin_data.k_plus = 0;