function out = analyze_cb_dx(data_file_string,varargin);
% Function returns data relating to cross-bridge distributions
% from simulations of filaments with distributed compliance

% Defaults
params.x = [-10:0.5:10];
params.no_of_heads = 18;

% Update
params = parse_pv_pairs(params,varargin);

% Code
d = load(data_file_string);
sim_data = d.d;

[no_of_time_steps,no_of_repeats]=size(sim_data.force);
for i=1:no_of_time_steps
    dx=[];
    for j=1:no_of_repeats
        dx = [dx cell2mat(sim_data.cb_dx(i,j))];
    end
    
%     if (i==200)
%         dx = dx;
%         figure(21);
%         clf;
%         [b,xn]=hist(dx,-10:0.5:10);
%         bar(xn,b);
%         error('ken');
%     end
    [n,x] = hist(dx,params.x);
    w(i,:) = n./(no_of_repeats*params.no_of_heads* ...
                    (params.x(2)-params.x(1)));
end

% Save data
out.x = x;
out.t = sim_data.t;
out.pops = w;
